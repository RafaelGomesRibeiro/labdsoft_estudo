package pt.ipp.isep.labdsoft.Estudo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import pt.ipp.isep.labdsoft.Estudo.dto.ReceitaPrescricaoDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Accessors(fluent = true)
@ToString
public final class ReceitaPrescricao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(EnumType.STRING)
    private Prescricao prescricao;
    private String descricao;
    private Long consultaId;

    public ReceitaPrescricao(Prescricao prescricao, String descricao, Long consultaId){
        this.prescricao = prescricao;
        this.descricao = descricao;
        this.consultaId = consultaId;
    }

    public ReceitaPrescricaoDTO toDTO() {
        return new ReceitaPrescricaoDTO(this.prescricao.toString(), this.descricao, this.consultaId);
    }

    public static ReceitaPrescricao fromDTO(ReceitaPrescricaoDTO dto){
        return new ReceitaPrescricao(Prescricao.valueOf(dto.prescricao.toUpperCase()), dto.descricao, dto.consultaId);
    }

}
