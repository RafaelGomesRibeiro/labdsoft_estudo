package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

public class Prediction {
    public String topIntent;
    public Entities entities;

    public String getTopIntent() {
        return topIntent;
    }

    public void setTopIntent(String topIntent) {
        this.topIntent = topIntent;
    }

    public Entities getEntities() {
        return entities;
    }

    public void setEntities(Entities entities) {
        this.entities = entities;
    }

    public Prediction(String topIntent, Entities entities) {
        this.topIntent = topIntent;
        this.entities = entities;
    }

    public Prediction() {
    }

    @Override
    public String toString() {
        return "Prediction{" +
                "topIntent='" + topIntent + '\'' +
                ", entities=" + entities +
                '}';
    }
}
