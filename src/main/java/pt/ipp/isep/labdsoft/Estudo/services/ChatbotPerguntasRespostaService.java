package pt.ipp.isep.labdsoft.Estudo.services;

public interface ChatbotPerguntasRespostaService {
    String resposta(String pergunta);
}
