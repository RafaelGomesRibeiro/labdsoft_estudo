package pt.ipp.isep.labdsoft.Estudo.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor

public class ReceitaPrescricaoDTO {
    public String prescricao;
    public String descricao;
    public Long consultaId;
}
