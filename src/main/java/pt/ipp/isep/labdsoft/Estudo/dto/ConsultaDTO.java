package pt.ipp.isep.labdsoft.Estudo.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public final class ConsultaDTO {
    public Long id;
    public Long medicoId;
    public Long utenteId;
    public String dataInicio;
    public String dor;
}
