package pt.ipp.isep.labdsoft.Estudo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RegistoDiarioDTO {
    public String data;
    public String sentimento;
    public String utenteId;
    public String descricao;
}
