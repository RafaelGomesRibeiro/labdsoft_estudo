package pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public final class EventDispatcher {
    private static final String ESTUDO_EXCHANGE_KEY = "estudo";
    private static final String CONSULTA_MARCADA_EVENT_KEY = "A";
    private static final String CONSULTA_ATUALIZADA_EVENT_KEY = "B";
    private static final String CONSULTA_CANCELADA_EVENT_KEY = "C";

    private RabbitTemplate template;

    public EventDispatcher(RabbitTemplate template) {
        this.template = template;
    }

    public void dispatchConsultaMarcadaEvent(ConsultaMarcadaEvent event) {
        template.convertAndSend(ESTUDO_EXCHANGE_KEY, CONSULTA_MARCADA_EVENT_KEY, event);
    }

    public void dispatchConsultaAtualizadaEvent(ConsultaAtualizadaEvent event) {
        template.convertAndSend(ESTUDO_EXCHANGE_KEY, CONSULTA_ATUALIZADA_EVENT_KEY, event);
    }

    public void dispatchConsultaCanceladaEvent(ConsultaCanceladaEvent event) {
        template.convertAndSend(ESTUDO_EXCHANGE_KEY, CONSULTA_CANCELADA_EVENT_KEY, event);
    }


}
