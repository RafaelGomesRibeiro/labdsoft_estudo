package pt.ipp.isep.labdsoft.Estudo.services;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
public final class ErrorDetails {
    public String errorMessage;
    public int statusCode;
}

