package pt.ipp.isep.labdsoft.Estudo.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.labdsoft.Estudo.domain.RegistoDiario;

public interface RegistoDiarioRepository extends JpaRepository<RegistoDiario, Long> {
}
