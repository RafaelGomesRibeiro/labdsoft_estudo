package pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ConsultaAtualizadaEvent implements Serializable {
    private String inicioAntigo;
    private String fimAntigo;
    private String inicioNovo;
    private String fimNovo;
    private String utenteId;
    private String medicoId;
}