package pt.ipp.isep.labdsoft.Estudo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.ipp.isep.labdsoft.Estudo.services.ChatbotPerguntasRespostaService;

@RestController
@RequestMapping("perguntasfrequentes")
public final class ChatbotPerguntasFrequentesController {

    private ChatbotPerguntasRespostaService chatbotPerguntasRespostaService;

    public ChatbotPerguntasFrequentesController(ChatbotPerguntasRespostaService chatbotPerguntasRespostaService){
        this.chatbotPerguntasRespostaService = chatbotPerguntasRespostaService;
    }

    @PostMapping(value="")
    public ResponseEntity<String> resposta(@RequestParam(value="pergunta") String pergunta) {
        return ResponseEntity.ok(chatbotPerguntasRespostaService.resposta(pergunta));
    }

}
