package pt.ipp.isep.labdsoft.Estudo.services;

import pt.ipp.isep.labdsoft.Estudo.dto.ConsultaDTO;

import java.util.List;

public interface ConsultaService {
    ConsultaDTO save(ConsultaDTO dto);
    void cancelar(Long consultaId);
    int registamMelhoria(List<Long> utentes);
    Iterable<ConsultaDTO> registoDores(Long utenteId);
    Iterable<ConsultaDTO> consultasUtente(Long utenteId);
    void atualizarDor(Long consultaId, String dorS);
}
