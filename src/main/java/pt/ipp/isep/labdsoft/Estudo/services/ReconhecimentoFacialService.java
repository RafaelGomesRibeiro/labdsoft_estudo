package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.web.multipart.MultipartFile;
import pt.ipp.isep.labdsoft.Estudo.domain.Sentimento;

public interface ReconhecimentoFacialService {
    Sentimento analisar(MultipartFile ficheiroStream);
}
