package pt.ipp.isep.labdsoft.Estudo.services;

import pt.ipp.isep.labdsoft.Estudo.dto.ReceitaPrescricaoDTO;

public interface ReceitaPrescricaoService {

    ReceitaPrescricaoDTO save(ReceitaPrescricaoDTO dto);
}
