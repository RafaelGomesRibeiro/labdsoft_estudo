package pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConsultaCanceladaEvent implements Serializable {
    private String inicio;
    private String fim;
    private String utenteId;
    private String medicoId;
}
