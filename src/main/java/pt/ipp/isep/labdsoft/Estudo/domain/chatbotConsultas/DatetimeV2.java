package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

import java.util.List;

public class DatetimeV2 {
    public String type;
    public List<Value> values;

    @Override
    public String toString() {
        return "DatetimeV2{" +
                "type='" + type + '\'' +
                ", values=" + values +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public DatetimeV2() {
    }

    public DatetimeV2(String type, List<Value> values) {
        this.type = type;
        this.values = values;
    }
}
