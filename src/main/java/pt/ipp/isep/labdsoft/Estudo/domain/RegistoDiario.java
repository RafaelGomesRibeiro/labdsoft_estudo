package pt.ipp.isep.labdsoft.Estudo.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Estudo.dto.RegistoDiarioDTO;
import pt.ipp.isep.labdsoft.Estudo.utils.DatesFormatter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistoDiario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long utenteId;
    @Enumerated(EnumType.STRING)
    private Sentimento sentimento;
    private LocalDateTime data;
    private String descricao;

    public RegistoDiarioDTO toDTO() {
        return new RegistoDiarioDTO(DatesFormatter.convertToString(data), sentimento != null ? sentimento.toString() : null, String.valueOf(utenteId), descricao);
    }
}
