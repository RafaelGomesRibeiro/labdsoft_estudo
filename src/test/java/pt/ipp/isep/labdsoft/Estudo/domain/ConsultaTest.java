package pt.ipp.isep.labdsoft.Estudo.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsultaTest {

    @Test
    public void efetuarConsulta(){
        LocalDateTime inicio = LocalDateTime.now();
        Consulta d = new Consulta(1L,1L,1L,inicio,null);
        //
        d.efetuar();

        Assertions.assertEquals(d.estado(), EstadoConsulta.EFETUADA);

    }


    @Test
    public void cancelarConsulta(){
        LocalDateTime inicio = LocalDateTime.now();
        Consulta d = new Consulta(1L,1L,1L,inicio,null);
        //
        d.cancelar();

        Assertions.assertEquals(d.estado(), EstadoConsulta.CANCELADA);

    }


    @Test
    public void invalidCancelarConsulta(){
        LocalDateTime inicio = LocalDateTime.now();
        Consulta d = new Consulta(1L,1L,1L,inicio,null);
        //
        d.cancelar();

        try{
            d.cancelar();
        }catch (Exception e){
            Assertions.assertEquals(e.getMessage(),"Esta consulta já se encontra cancelada");
        }


    }



    @Test
    public void marcarConsulta(){
        LocalDateTime inicio = LocalDateTime.now();
        Consulta d = new Consulta(1L,1L,1L,inicio,null);
        //
        d.marcar();

        Assertions.assertEquals(d.estado(), EstadoConsulta.MARCADA);

    }

}
