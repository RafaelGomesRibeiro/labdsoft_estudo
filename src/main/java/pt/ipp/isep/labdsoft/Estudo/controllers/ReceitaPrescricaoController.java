package pt.ipp.isep.labdsoft.Estudo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.ipp.isep.labdsoft.Estudo.dto.ReceitaPrescricaoDTO;
import pt.ipp.isep.labdsoft.Estudo.services.ReceitaPrescricaoService;

@RestController
@RequestMapping("receitasPrescricao")
public class ReceitaPrescricaoController {

    private final ReceitaPrescricaoService receitaPrescricaoService;

    public ReceitaPrescricaoController(ReceitaPrescricaoService service){
        this.receitaPrescricaoService = service;
    }

    @PostMapping("/")
    public ResponseEntity<ReceitaPrescricaoDTO> create(@RequestBody ReceitaPrescricaoDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON).body(receitaPrescricaoService.save(dto));
    }
}
