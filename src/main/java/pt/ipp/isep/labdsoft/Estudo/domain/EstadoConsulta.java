package pt.ipp.isep.labdsoft.Estudo.domain;

public enum EstadoConsulta {
    CANCELADA,
    MARCADA,
    EFETUADA
}
