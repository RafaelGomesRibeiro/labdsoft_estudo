package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Estudo.dto.ChatbotPerguntasPedidoDTO;
import pt.ipp.isep.labdsoft.Estudo.dto.ChatbotPerguntasRespostaDTO;


@Service
public class ChatbotPerguntasRespostaServiceImpl  implements ChatbotPerguntasRespostaService {

    private final WebClient webClient;

    @Value("${AZURE_SUBSCRIPTION_KEY_QnA}")
    private String azureSubscription;

    private static final String AUTHORIZATION_HEADER = "Ocp-Apim-Subscription-Key";
    private static final String ENDPOINT_URL = "https://inovcli.cognitiveservices.azure.com/qnamaker/v5.0-preview.1/knowledgebases/831e4252-311a-46e2-bd39-d934f0b3708a/generateAnswer";

    public ChatbotPerguntasRespostaServiceImpl(){
        this.webClient = WebClient.create();
    }

    @Override
    public String resposta(String pergunta) {
        ChatbotPerguntasPedidoDTO perguntaEnviar = new ChatbotPerguntasPedidoDTO(pergunta);
        try{
            ChatbotPerguntasRespostaDTO resposta = this.webClient.post().uri(ENDPOINT_URL).header(AUTHORIZATION_HEADER, azureSubscription)
                    .contentType(MediaType.APPLICATION_JSON).bodyValue(perguntaEnviar).retrieve().bodyToMono(ChatbotPerguntasRespostaDTO.class).block();

            if(resposta.answers.get(0).answer.equalsIgnoreCase("No good match found in KB")){
                  return "Desculpe, não percebi a pergunta. Pode repetir?";
            } else {
                return resposta.answers.get(0).answer;
            }
        }catch (Exception e) {
            System.out.println("Este foi o erro: " + e.getMessage());
        }
        return null;
    }
}
