package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class ChatMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long utenteId;
    private boolean bot;
    private String message;
    private LocalDateTime date;

    public ChatMessage(Long utenteId, boolean bot, String message, LocalDateTime date) {
        this.utenteId = utenteId;
        this.bot = bot;
        this.message = message;
        this.date = date;
    }
}
