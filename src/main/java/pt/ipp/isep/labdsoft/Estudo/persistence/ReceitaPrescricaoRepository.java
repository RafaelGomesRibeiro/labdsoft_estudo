package pt.ipp.isep.labdsoft.Estudo.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Estudo.domain.ReceitaPrescricao;

@Repository

public interface ReceitaPrescricaoRepository extends JpaRepository<ReceitaPrescricao, Long>{
}
