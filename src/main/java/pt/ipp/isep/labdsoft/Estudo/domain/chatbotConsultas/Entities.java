package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

import java.util.List;

public class Entities {
    public List<DatetimeV2> datetimeV2;

    public List<DatetimeV2> getDatetimeV2() {
        return datetimeV2;
    }

    public void setDatetimeV2(List<DatetimeV2> datetimeV2) {
        this.datetimeV2 = datetimeV2;
    }

    public Entities() {
    }

    public Entities(List<DatetimeV2> datetimeV2) {
        this.datetimeV2 = datetimeV2;
    }
}
