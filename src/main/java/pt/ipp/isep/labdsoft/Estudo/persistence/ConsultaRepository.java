package pt.ipp.isep.labdsoft.Estudo.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.Estudo.domain.Consulta;
import pt.ipp.isep.labdsoft.Estudo.domain.EstadoConsulta;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ConsultaRepository extends JpaRepository<Consulta, Long> {
    List<Consulta> findConsultasByUtenteId(Long idUtente);
    List<Consulta> findConsultasByUtenteIdAndEstado(Long idUtente, EstadoConsulta estadoConsulta);
    List<Consulta> findConsultasByUtenteIdOrderByInicio(Long idUtente);
    Optional<Consulta> findConsultaByInicioAndUtenteIdAndEstado(LocalDateTime Inicio, Long utenteId, EstadoConsulta es);
}
