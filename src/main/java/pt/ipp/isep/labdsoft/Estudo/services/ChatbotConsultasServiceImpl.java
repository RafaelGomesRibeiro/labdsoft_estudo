package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Estudo.domain.Consulta;
import pt.ipp.isep.labdsoft.Estudo.domain.EstadoConsulta;
import pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas.ChatMessage;
import pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas.DatetimeV2;
import pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas.ResponseLUIS;
import pt.ipp.isep.labdsoft.Estudo.dto.ChatbotPerguntasRespostaDTO;
import pt.ipp.isep.labdsoft.Estudo.dto.ConsultaDTO;
import pt.ipp.isep.labdsoft.Estudo.persistence.ChatMessageRepository;
import pt.ipp.isep.labdsoft.Estudo.persistence.ConsultaRepository;
import pt.ipp.isep.labdsoft.Estudo.utils.DatesFormatter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChatbotConsultasServiceImpl implements ChatbotConsultasService {

    private final WebClient webClient;
    @Value("${MEDICOS_BASE_URL}")
    private String medicosBaseUrl;
    @Autowired
    private ConsultaService consultaService;
    @Autowired
    private ConsultaRepository repoConsultas;

    private static final String ENDPOINT_URL = "https://westus.api.cognitive.microsoft.com/luis/prediction/v3.0/apps/4560376d-c49f-48e5-94ba-058bb9a23a7d/slots/staging/predict?subscription-key=acdfa5ef254941599bd45eebe29d709c&verbose=true&show-all-intents=true&log=true&query=";
    private LocalDateTime dataCancelamento;

    public ChatbotConsultasServiceImpl() {
        this.webClient = WebClient.create();
    }

    @Override
    public String resposta(String pergunta, Long utenteId) {
        ResponseLUIS resposta = null;
        try {
            resposta = this.webClient.get().uri(ENDPOINT_URL + pergunta)
                    .retrieve().bodyToMono(ResponseLUIS.class).block();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "Lamento, mas neste momento o sistema está com problemas técnicos inesperados!";
        }


        String intent = resposta.getPrediction().getTopIntent();
        if (intent.equals("AlterarConsulta")) {
            LocalDateTime dataAntiga = LocalDateTime.now();
            LocalDateTime dataNova = LocalDateTime.now();
            try {
                dataAntiga = DatesFormatter.convertToLocalDateTimeWithSecs(resposta.getPrediction().getEntities().getDatetimeV2().get(0).getValues().get(0).getResolution().get(0).getValue());
                dataNova = DatesFormatter.convertToLocalDateTimeWithSecs(resposta.getPrediction().getEntities().getDatetimeV2().get(1).getValues().get(0).getResolution().get(0).getValue());
            } catch (Exception e) {
                System.out.println(e.getMessage());

                return "Lamento mas não consigo realizar alteração para as datas indicadas.\nDeve indicar a data e hora original da consulta e o novo horário num formato legível.";
            }
            if(!validarData(dataAntiga)) return "Lamento, mas a data da marcação não é válida, apenas são permitdas consultas ao inicio, ou a meio de uma hora (ex: 13:00:00). Deseja fazer algo mais?";
            if(!validarData(dataNova)) return "Lamento, mas a data da marcação não é válida, apenas são permitdas consultas ao inicio, ou a meio de uma hora (ex: 13:00:00). Deseja fazer algo mais?";

            return handleAlteracao(utenteId,dataAntiga,dataNova);

        } else if (intent.equals("CancelarConsulta")) {
            LocalDateTime dataCancelamento = LocalDateTime.now();
            try {
                dataCancelamento = DatesFormatter.convertToLocalDateTimeWithSecs(resposta.getPrediction().getEntities().getDatetimeV2().get(0).getValues().get(0).getResolution().get(0).getValue());
            } catch (Exception e) {
                System.out.println(e.getMessage());

                return "Lamento mas não consigo realizar o cancelamento para essa data.\nDeve indicar uma data e hora de cancelamento num formato legível.";
            }
            if(!validarData(dataCancelamento)) return "Lamento, mas a data da marcação não é válida, apenas são permitdas consultas ao inicio, ou a meio de uma hora (ex: 13:00:00). Deseja fazer algo mais?";

            return handleCancelamento(utenteId,dataCancelamento);

        } else if (intent.equals("MarcarConsulta")) {
            LocalDateTime dataMarcacao = LocalDateTime.now();
            try {
                dataMarcacao = DatesFormatter.convertToLocalDateTimeWithSecs(resposta.getPrediction().getEntities().getDatetimeV2().get(0).getValues().get(0).getResolution().get(0).getValue());
            } catch (Exception e) {
                System.out.println(e.getMessage());

                return "Lamento mas não consigo realizar a marcação para essa data.\nDeve indicar uma data e hora da mesma num formato legível.";
            }
            if(!validarData(dataMarcacao)) return "Lamento, mas a data da marcação não é válida, apenas são permitdas consultas ao inicio, ou a meio de uma hora (ex: 13:00:00). Deseja fazer algo mais?";
            return handleCriação(utenteId,dataMarcacao);
        }

        return "Lamento, não compreendi o seu pedido.\nEste chat permite a Criação/Alteração/Cancelamento de consultas, o que deseja?";
    }

    @Override
    public String inicioConversa(Long utenteId) {
        //TODO meter na UI
        String messageGreet = "Seja bem vindo ao Chat de gestão de consultas.\n" +
                "Eu sou o InovBot e aqui poderá fazer a Marcação/Alteração/Cancelamento das suas consultas.\n" +
                "Para isso apenas necessita de me indicar a opção pretendida e a data e hora. O que deseja?";

        return messageGreet;
    }


    private String handleAlteracao(Long utenteId, LocalDateTime original, LocalDateTime nova) {
        Consulta c =  repoConsultas.findConsultaByInicioAndUtenteIdAndEstado(original,utenteId, EstadoConsulta.MARCADA).orElse(null);
        if(c==null) return "A alteração não é válida.\nLamento, mas não consigo encontrar nenhuma consulta para "+original+", que pretende fazer?";
    //TODO As consultas ja marcadas podem ser alteradas???
        try {
            consultaService.save(new ConsultaDTO(c.id(), c.medicoId(), c.utenteId(), DatesFormatter.convertToString(nova), null));
        }catch (Exception e){
            e.printStackTrace();
            return "Lamento mas não é possivel realizar a marcação de consulta para a nova data ("+nova+"), pretende realizar outra operação?";
        }
        return "A sua consulta de "+original+" foi alterada para "+nova+".\nCaso pretenda realizar outra operação esteja à vontade!";
    }

    private String handleCancelamento(Long utenteId, LocalDateTime date) {
        Consulta c =  repoConsultas.findConsultaByInicioAndUtenteIdAndEstado(date,utenteId, EstadoConsulta.MARCADA).orElse(null);
      if(c==null) return "O cancelamento não é válido.\nLamento, mas não consigo encontrar nenhuma consulta para "+date+", que pretende fazer?";

      try {
          consultaService.cancelar(c.id());
          return "A sua consulta marcada para "+ date+" está cancelada!\nSe pretender realizar outra operação esteja à vontade!";
      }catch (Exception e){
          return "Lamento, mas não estou a conseguir realizar o cancelamento.\nQue pretende fazer?";
      }
    }

    private String handleCriação(Long utenteId, LocalDateTime date) {

        Long idMedico = -1L;
        try {
            idMedico = this.webClient.get().uri(medicosBaseUrl + "/findMedicoDisponivel?data="+DatesFormatter.convertToString(date)+"&idUtente="+utenteId)
                    .retrieve().bodyToMono(Long.class).block();
        }catch (Exception e){
            e.printStackTrace();
            idMedico = -1L;
        }
        if(idMedico == -1L) return "Lamento, mas não consegui realizar a marcação para esta data. Que operação pretende fazer";

        try {
            ConsultaDTO c = new ConsultaDTO(null, idMedico, utenteId, DatesFormatter.convertToString(date), null);
            consultaService.save(c);
            return "Tem uma nova consulta para " + date + ".\nCaso pretenda realizar outra operação esteja à vontade!";
        }catch (Exception e){
            return e.getMessage() + "\nA consulta não pode ser marcada, que operação deseja realizar?";
        }
    }

    private boolean validarData(LocalDateTime d){
        System.out.println(d.getMinute()+" "+d.getSecond());
        if(d.getMinute() != 0 && d.getMinute() != 30) return false;
        if(d.getSecond() != 0) return false;
        return true;
    }

}
