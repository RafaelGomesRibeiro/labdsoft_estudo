package pt.ipp.isep.labdsoft.Estudo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Estudo.dto.ConsultaDTO;
import pt.ipp.isep.labdsoft.Estudo.services.ConsultaService;

import java.util.List;

@RestController
@RequestMapping("consultas")
public final class ConsultaController {

    private final ConsultaService consultaService;

    public ConsultaController(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    @PostMapping("/")
    public ResponseEntity<ConsultaDTO> create(@RequestBody ConsultaDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON).body(consultaService.save(dto));
    }

    @PutMapping("/reagendar/{id}")
    public ResponseEntity<ConsultaDTO> update(@PathVariable Long id, @RequestBody ConsultaDTO dto) {
        dto.id = id;
        return create(dto);
    }

    @PutMapping("")
    public ResponseEntity<?> updateDor(@RequestParam(value = "consultaId") Long consultaId, @RequestParam(value = "dor") String dor) {
        consultaService.atualizarDor(consultaId, dor);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    @PutMapping("/cancelar/{id}")
    public ResponseEntity<?> cancelar(@PathVariable Long id) {
        consultaService.cancelar(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/melhorias/")
    public ResponseEntity<Integer> melhorias(@RequestBody List<Long> utentes) {
        return ResponseEntity.ok(consultaService.registamMelhoria(utentes));
    }

    @GetMapping("/dores/{utenteId}")
    public ResponseEntity<Iterable<ConsultaDTO>> registoDores(@PathVariable Long utenteId) {
        return ResponseEntity.ok(consultaService.registoDores(utenteId));
    }

    @GetMapping("/utente/{utenteId}")
    public ResponseEntity<Iterable<ConsultaDTO>> consultasPorUtente(@PathVariable Long utenteId) {
        return ResponseEntity.ok(consultaService.consultasUtente(utenteId));
    }
}
