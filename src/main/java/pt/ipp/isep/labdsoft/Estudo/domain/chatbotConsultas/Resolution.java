package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

public class Resolution {
    public String value;

    @Override
    public String toString() {
        return "Resolution{" +
                "value='" + value + '\'' +
                '}';
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Resolution() {
    }

    public Resolution(String value) {
        this.value = value;
    }
}
