package pt.ipp.isep.labdsoft.Estudo.dto;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class ChatbotPerguntasPedidoDTO {

    public String question;

}
