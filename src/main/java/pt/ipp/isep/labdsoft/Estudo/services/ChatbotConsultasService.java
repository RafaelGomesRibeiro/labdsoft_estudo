package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.web.bind.annotation.RequestParam;
import pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas.ChatMessage;

import java.util.List;

public interface ChatbotConsultasService {

    String resposta(String pergunta, Long utenteId);

   String inicioConversa(Long utenteId);

}
