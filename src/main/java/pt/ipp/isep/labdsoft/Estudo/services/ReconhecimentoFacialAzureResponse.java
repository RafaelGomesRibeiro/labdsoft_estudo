package pt.ipp.isep.labdsoft.Estudo.services;

import lombok.ToString;

@ToString
public class ReconhecimentoFacialAzureResponse {
    public FaceAttributes faceAttributes;

    @ToString
    public static final class Emotion {
        public double anger;
        public double contempt;
        public double disgust;
        public double fear;
        public double happiness;
        public double neutral;
        public double sadness;
        public double surprise;
    }

    @ToString
    public static final class FaceAttributes {
        public Emotion emotion;
    }
}
