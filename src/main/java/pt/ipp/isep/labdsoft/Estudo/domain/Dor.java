package pt.ipp.isep.labdsoft.Estudo.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.Embeddable;

@EqualsAndHashCode
@Embeddable
@Getter
public final class Dor {

    protected Dor() {
        dor = null;
    }

    private static final Short DOR_MINIMA = 1;
    private static final Short DOR_MAXIMA = 10;

    private final Short dor;

    private Dor(final String dorS) {
        this.dor = parse(dorS);
    }

    private Short parse(final String dorS) {
        Short dor;
        try {
            dor = Short.parseShort(dorS);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("O nível de dor tem que ser um número");
        }
        if (dor < DOR_MINIMA) {
            throw new IllegalArgumentException(String.format("A dor não pode ser inferior a %d", DOR_MINIMA));
        }
        if (dor > DOR_MAXIMA) {
            throw new IllegalArgumentException(String.format("A dor não pode ser superior a %d", DOR_MAXIMA));
        }
        return dor;
    }

    public static Dor valueOf(String dorS) {
        return new Dor(dorS);
    }

    @Override
    public String toString() {
        return String.valueOf(dor);
    }
}
