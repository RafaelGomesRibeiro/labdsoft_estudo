package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

public class ResponseLUIS {
        public String query;
        public Prediction prediction;

    public ResponseLUIS(String query, Prediction prediction) {
        this.query = query;
        this.prediction = prediction;
    }

    public ResponseLUIS() {
    }

    public String getQuery() {
        return query;
    }

    @Override
    public String toString() {
        return "responseLUIS{" +
                "query='" + query + '\'' +
                ", prediction=" + prediction +
                '}';
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Prediction getPrediction() {
        return prediction;
    }

    public void setPrediction(Prediction prediction) {
        this.prediction = prediction;
    }
}
