package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Estudo.domain.Sentimento;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class ReconhecimentoFacialAzure implements ReconhecimentoFacialService {

    private final WebClient webClient;


    @Value("${AZURE_SUBSCRIPTION_KEY}")
    private String azureSubscription;

    private static final String AUTHORIZATION_HEADER = "Ocp-Apim-Subscription-Key";
    private static final String ENDPOINT_URL = "https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=emotion&recognitionModel=recognition_03&returnRecognitionModel=false&detectionModel=detection_01&faceIdTimeToLive=86400";
    private static final Map<Sentimento, Double> sentimentos = new LinkedHashMap<>();

    public ReconhecimentoFacialAzure() {
        this.webClient = WebClient.create();
    }

    @Override
    public Sentimento analisar(MultipartFile ficheiro) {
        try {
            final ReconhecimentoFacialAzureResponse[] resultadoArray = this.webClient.post().uri(ENDPOINT_URL).header(AUTHORIZATION_HEADER, azureSubscription)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM).bodyValue(ficheiro.getBytes()).retrieve()
                    .bodyToMono(ReconhecimentoFacialAzureResponse[].class).block();
            if (resultadoArray == null || resultadoArray.length == 0) {
                throw new IllegalArgumentException("Não foi reconhecida nenhuma face na fotografia");
            }
            ReconhecimentoFacialAzureResponse resultado = resultadoArray[0];
            sentimentos.put(Sentimento.RAIVA, resultado.faceAttributes.emotion.anger);
            sentimentos.put(Sentimento.FELICIDADE, resultado.faceAttributes.emotion.happiness);
            sentimentos.put(Sentimento.NEUTRO, resultado.faceAttributes.emotion.neutral);
            sentimentos.put(Sentimento.TRISTEZA, resultado.faceAttributes.emotion.sadness);
            sentimentos.put(Sentimento.SURPRESA, resultado.faceAttributes.emotion.surprise);
            sentimentos.put(Sentimento.MEDO, resultado.faceAttributes.emotion.fear);
            sentimentos.put(Sentimento.REPULSA, resultado.faceAttributes.emotion.disgust);
            sentimentos.put(Sentimento.DESDEM, resultado.faceAttributes.emotion.contempt);
            return Collections.max(sentimentos.entrySet(), Map.Entry.comparingByValue()).getKey();
        } catch (IOException e) {
            System.out.println("Este foi o erro: ssssss" + e.getMessage());
        }
        return null;
    }
}
