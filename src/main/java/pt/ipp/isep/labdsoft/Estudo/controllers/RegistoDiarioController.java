package pt.ipp.isep.labdsoft.Estudo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pt.ipp.isep.labdsoft.Estudo.dto.RegistoDiarioDTO;
import pt.ipp.isep.labdsoft.Estudo.services.RegistoDiarioService;

@RestController
@RequestMapping("registodiario")
public final class RegistoDiarioController {

    private RegistoDiarioService registoDiarioService;

    public RegistoDiarioController(RegistoDiarioService registoDiarioService) {
        this.registoDiarioService = registoDiarioService;
    }


    @PostMapping(value = "")
    public ResponseEntity<RegistoDiarioDTO> create(@RequestPart(value = "foto", required = false) MultipartFile file, @RequestParam(name = "texto", required = false) String texto,
                                                   @RequestParam(value = "utenteId") Long utenteId) {
        RegistoDiarioDTO res = registoDiarioService.save(file, texto, utenteId);
        return ResponseEntity.ok(res);
    }
}
