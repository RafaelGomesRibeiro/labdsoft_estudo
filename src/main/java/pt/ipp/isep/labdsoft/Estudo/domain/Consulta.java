package pt.ipp.isep.labdsoft.Estudo.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import pt.ipp.isep.labdsoft.Estudo.dto.ConsultaDTO;
import pt.ipp.isep.labdsoft.Estudo.utils.DatesFormatter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Accessors(fluent = true)
@ToString
public final class Consulta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long medicoId;
    private Long utenteId;
    private LocalDateTime inicio;
    @Enumerated(EnumType.STRING)
    private EstadoConsulta estado;
    private Dor dor;

    public static final Integer DURACAO_CONSULTA_MINUTOS = 30;

    public Consulta(Long id, Long medicoId, Long utenteId, LocalDateTime inicio, Dor dor) {
        this.id = id;
        this.medicoId = medicoId;
        this.utenteId = utenteId;
        this.inicio = inicio;
        this.dor = dor;
    }


    public ConsultaDTO toDTO() {
        return new ConsultaDTO(id, medicoId, utenteId, DatesFormatter.convertToString(inicio), dor != null ? dor.toString() : null);
    }

    public static Consulta fromDTO(ConsultaDTO dto) {
        return new Consulta(dto.id, dto.medicoId, dto.utenteId, DatesFormatter.convertToLocalDateTime(dto.dataInicio), dto.dor != null ? Dor.valueOf(dto.dor) : null);
    }

    public void efetuar() {
        estado = EstadoConsulta.EFETUADA;
    }

    public void cancelar() {
        if (estado == EstadoConsulta.CANCELADA)
            throw new IllegalArgumentException("Esta consulta já se encontra cancelada");
        estado = EstadoConsulta.CANCELADA;
    }

    public void marcar() {
        estado = EstadoConsulta.MARCADA;
    }

    public boolean isCancelada() {
        return estado == EstadoConsulta.CANCELADA;
    }

    public boolean isEfetuada() {
        return estado == EstadoConsulta.EFETUADA;
    }

}
