package pt.ipp.isep.labdsoft.Estudo.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas.ChatMessage;
import pt.ipp.isep.labdsoft.Estudo.services.ChatbotConsultasService;

import java.util.List;

@RestController
@RequestMapping("botconsultas")
public class ChatbotConsultasController {
    @Autowired
    private ChatbotConsultasService chatbotConsultasService;

    @GetMapping("/novasessao")
    public ResponseEntity<String> inicioConversa(@RequestParam(value="utenteId") Long utenteId){
        return ResponseEntity.ok(chatbotConsultasService.inicioConversa(utenteId));
    }

    @PostMapping("/")
    public ResponseEntity<String> pedido(@RequestParam(value = "pergunta") String pergunta,@RequestParam(value = "utenteId") Long utenteId) {
        return ResponseEntity.ok(chatbotConsultasService.resposta(pergunta, utenteId));
    }

}
