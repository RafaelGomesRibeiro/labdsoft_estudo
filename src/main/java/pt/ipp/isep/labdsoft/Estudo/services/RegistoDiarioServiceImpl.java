package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Estudo.domain.RegistoDiario;
import pt.ipp.isep.labdsoft.Estudo.domain.Sentimento;
import pt.ipp.isep.labdsoft.Estudo.dto.RegistoDiarioDTO;
import pt.ipp.isep.labdsoft.Estudo.persistence.RegistoDiarioRepository;

import java.net.URI;
import java.time.LocalDateTime;

@Service
public final class RegistoDiarioServiceImpl implements RegistoDiarioService {

    private RegistoDiarioRepository repository;
    private ReconhecimentoFacialService reconhecimentoFacialService;

    @Value("${UTENTES_BASE_URL}")
    private String utentesBaseUrl;

    private final WebClient webClient;

    public RegistoDiarioServiceImpl(RegistoDiarioRepository repository, ReconhecimentoFacialService reconhecimentoFacialService) {
        this.repository = repository;
        this.reconhecimentoFacialService = reconhecimentoFacialService;
        this.webClient = WebClient.create();
    }

    @Override
    public RegistoDiarioDTO save(MultipartFile file, String text, Long utenteId) {
        final Boolean utenteAceite = webClient.get().uri(URI.create(utentesBaseUrl + "/isAccepted/" + utenteId)).retrieve()
                .bodyToMono(Boolean.class).block();
        if (!utenteAceite) throw new IllegalArgumentException("Utente inválido");
        Sentimento sentimento = null;
        if (file != null) {
            sentimento = reconhecimentoFacialService.analisar(file);
        }
        RegistoDiario registoDiario = RegistoDiario.builder().sentimento(sentimento)
                .utenteId(utenteId).data(LocalDateTime.now()).descricao(text).build();
        repository.save(registoDiario);
        return registoDiario.toDTO();
    }
}
