package pt.ipp.isep.labdsoft.Estudo.domain;

public enum Sentimento {
    RAIVA {
        @Override
        public String toString() {
            return "Raiva";
        }
    },
    DESDEM {
        @Override
        public String toString() {
            return "Desdém";
        }
    },
    REPULSA {
        @Override
        public String toString() {
            return "Repulsa";
        }
    },
    MEDO {
        @Override
        public String toString() {
            return "Medo";
        }
    },
    FELICIDADE {
        @Override
        public String toString() {
            return "Felicidade";
        }
    },
    NEUTRO {
        @Override
        public String toString() {
            return "Neutro";
        }
    },
    TRISTEZA {
        @Override
        public String toString() {
            return "Tristeza";
        }
    },
    SURPRESA {
        @Override
        public String toString() {
            return "Surpreso";
        }
    }
}
