package pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class ConsultaMarcadaEvent implements Serializable {
    private String inicio;
    private String fim;
    private String utenteId;
    private String medicoId;
}
