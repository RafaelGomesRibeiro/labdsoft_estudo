package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.web.multipart.MultipartFile;
import pt.ipp.isep.labdsoft.Estudo.dto.RegistoDiarioDTO;

public interface RegistoDiarioService {
    RegistoDiarioDTO save(MultipartFile file, String text, Long utenteId);
}
