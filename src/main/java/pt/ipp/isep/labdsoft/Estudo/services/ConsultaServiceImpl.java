package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Estudo.domain.Consulta;
import pt.ipp.isep.labdsoft.Estudo.domain.Dor;
import pt.ipp.isep.labdsoft.Estudo.domain.EstadoConsulta;
import pt.ipp.isep.labdsoft.Estudo.dto.ConsultaDTO;
import pt.ipp.isep.labdsoft.Estudo.persistence.ConsultaRepository;
import pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher.ConsultaAtualizadaEvent;
import pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher.ConsultaCanceladaEvent;
import pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher.ConsultaMarcadaEvent;
import pt.ipp.isep.labdsoft.Estudo.services.rabbitMQ.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.Estudo.utils.DatesFormatter;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public final class ConsultaServiceImpl implements ConsultaService {

    @Value("${MEDICOS_BASE_URL}")
    private String medicosBaseUrl;
    private final ConsultaRepository consultaRepository;
    private final WebClient webClient;
    private final EventDispatcher dispatcher;

    public ConsultaServiceImpl(ConsultaRepository consultaRepository, EventDispatcher dispatcher) {
        this.consultaRepository = consultaRepository;
        this.webClient = WebClient.create();
        this.dispatcher = dispatcher;
    }


    @Override
    public ConsultaDTO save(ConsultaDTO dto) {
        Consulta consultaAntiga = dto.id == null ? null : consultaRepository.findById(dto.id).orElse(null);
        final String inicioAntigo = consultaAntiga == null ? null : DatesFormatter.convertToString(consultaAntiga.inicio());
        final String fimAntigo = consultaAntiga == null ? null : DatesFormatter.convertToString(consultaAntiga.inicio().plusMinutes(Consulta.DURACAO_CONSULTA_MINUTOS));
        Consulta c = Consulta.fromDTO(dto);
        c.marcar();
        String endpoint = medicosBaseUrl + String.format("/disponibilidade?id=%s&data=%s&idUtente=%s", dto.medicoId, dto.dataInicio, dto.utenteId);
        endpoint = endpoint.replaceAll(" ", "%20");
        final Boolean disponibilidade = webClient.get().uri(URI.create(endpoint)).retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> clientResponse.bodyToMono(ErrorDetails.class).flatMap(
                        msg -> {
                            return Mono.error(new IllegalArgumentException(msg.getErrorMessage()));
                        }
                )).bodyToMono(Boolean.class).block();
        if (!disponibilidade)
            throw new IllegalArgumentException("Utente não se encontra disponível para a hora " + dto.dataInicio);
        c = consultaRepository.save(c);
        final LocalDateTime fim = c.inicio().plusMinutes(Consulta.DURACAO_CONSULTA_MINUTOS);
        CompletableFuture.runAsync(() -> {
            if (consultaAntiga == null) {
                ConsultaMarcadaEvent e = ConsultaMarcadaEvent.builder().inicio(dto.dataInicio).medicoId(String.valueOf(dto.medicoId))
                        .utenteId(String.valueOf(dto.utenteId)).fim(DatesFormatter.convertToString(fim)).build();
                dispatcher.dispatchConsultaMarcadaEvent(e);
            } else {
                if (consultaAntiga.isCancelada() || consultaAntiga.isEfetuada()) {
                    throw new IllegalArgumentException(String.format("Consulta com o id %d já foi %s", dto.id, consultaAntiga.isEfetuada() ? "efetuada" : "cancelada"));
                }
                ConsultaAtualizadaEvent e = ConsultaAtualizadaEvent.builder().inicioAntigo(inicioAntigo).fimAntigo(fimAntigo)
                        .inicioNovo(dto.dataInicio).fimNovo(DatesFormatter.convertToString(fim)).medicoId(String.valueOf(dto.medicoId)).utenteId(String.valueOf(dto.utenteId)).build();
                System.out.println(e);
                dispatcher.dispatchConsultaAtualizadaEvent(e);
            }
            System.out.println("lancei o evento");
        });
        return c.toDTO();
    }

    @Override
    public void cancelar(Long consultaId) {
        final Consulta consulta = consultaRepository.findById(consultaId).orElseThrow(() -> new NoSuchElementException(String.format("Consulta com o id %s não existe", consultaId)));
        consulta.cancelar();
        consultaRepository.save(consulta);
        CompletableFuture.runAsync(() -> {
            ConsultaCanceladaEvent e = ConsultaCanceladaEvent.builder().utenteId(String.valueOf(consulta.utenteId())).medicoId(String.valueOf(consulta.medicoId()))
                    .inicio(DatesFormatter.convertToString(consulta.inicio())).fim(DatesFormatter.convertToString(consulta.inicio().plusMinutes(Consulta.DURACAO_CONSULTA_MINUTOS)))
                    .build();
            dispatcher.dispatchConsultaCanceladaEvent(e);
        });
    }

    @Override
    public int registamMelhoria(List<Long> utentes) {
        int melhoria = 0;
        for (Long id : utentes) {
            List<Consulta> consultas = consultaRepository.findConsultasByUtenteId(id);
            if(consultas.size()==0) continue;
            //TODO: Organizar consultas por ordem cronologica
            int dorInicial = consultas.get(0).dor().getDor();
            int dorFinal = consultas.get(consultas.size() - 1).dor().getDor();
            if (dorFinal < dorInicial) melhoria++;
        }
        return melhoria;
    }

    @Override
    public Iterable<ConsultaDTO> registoDores(Long utenteId) {
        return consultaRepository.findConsultasByUtenteIdOrderByInicio(utenteId).stream().filter(c -> c.dor() != null).map(Consulta::toDTO).collect(Collectors.toList());
    }

    @Override
    public Iterable<ConsultaDTO> consultasUtente(Long utenteId) {
        return consultaRepository.findConsultasByUtenteIdAndEstado(utenteId, EstadoConsulta.MARCADA).stream().filter(c -> c.dor() == null).map(Consulta::toDTO).collect(Collectors.toList());
    }

    @Override
    public void atualizarDor(Long consultaId, String dorS) {
        Consulta consulta = consultaRepository.findById(consultaId).orElseThrow(() -> new NoSuchElementException(String.format("Consulta com o id %d não existe", consultaId)));
        Dor dor = Dor.valueOf(dorS);
        consulta.efetuar();
        consulta.dor(dor);
        consultaRepository.save(consulta);
    }
}
