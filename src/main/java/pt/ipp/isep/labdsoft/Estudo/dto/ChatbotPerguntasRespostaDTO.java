package pt.ipp.isep.labdsoft.Estudo.dto;

import lombok.ToString;

import java.util.List;

@ToString
public class ChatbotPerguntasRespostaDTO {

    public List<Answer> answers;


    @ToString
    public static final class Answer{
        public String answer;
    }
}
