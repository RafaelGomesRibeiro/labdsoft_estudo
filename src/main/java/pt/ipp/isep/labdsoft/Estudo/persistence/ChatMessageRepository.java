package pt.ipp.isep.labdsoft.Estudo.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas.ChatMessage;

import java.util.List;

@Repository
@Transactional
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {
    List<ChatMessage> findAllByUtenteIdOrderByDateAsc(Long utenteId);
    void deleteAllByUtenteId(Long utenteId);
}
