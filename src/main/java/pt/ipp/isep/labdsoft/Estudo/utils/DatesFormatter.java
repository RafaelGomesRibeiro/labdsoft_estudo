package pt.ipp.isep.labdsoft.Estudo.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DatesFormatter {
    public static final String FORMATO_DATA = "dd-MM-yyyy HH:mm";
    public static final String FORMATO_DATA_WITH_SECONDS = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(FORMATO_DATA);
    private static final DateTimeFormatter FORMATTER_SEC = DateTimeFormatter.ofPattern(FORMATO_DATA_WITH_SECONDS);

    private DatesFormatter() {
    }
    public static String convertToString(LocalDateTime time) {
        return time.format(FORMATTER);
    }
    public static LocalDateTime convertToLocalDateTime(String time) {
        return LocalDateTime.parse(time, FORMATTER);
    }
    public static LocalDateTime convertToLocalDateTimeWithSecs(String time) {
        return LocalDateTime.parse(time, FORMATTER_SEC);
    }

}
