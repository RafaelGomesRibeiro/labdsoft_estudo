package pt.ipp.isep.labdsoft.Estudo.domain.chatbotConsultas;

import java.util.List;

public class Value {
    public String timex;
    public List<Resolution> resolution;

    public Value() {
    }

    public Value(String timex, List<Resolution> resolution) {
        this.timex = timex;
        this.resolution = resolution;
    }

    public String getTimex() {
        return timex;
    }

    public void setTimex(String timex) {
        this.timex = timex;
    }

    public List<Resolution> getResolution() {
        return resolution;
    }

    public void setResolution(List<Resolution> resolution) {
        this.resolution = resolution;
    }

    @Override
    public String toString() {
        return "Value{" +
                "timex='" + timex + '\'' +
                ", resolution=" + resolution +
                '}';
    }
}
