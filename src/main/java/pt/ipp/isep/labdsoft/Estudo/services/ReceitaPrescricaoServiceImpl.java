package pt.ipp.isep.labdsoft.Estudo.services;

import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Estudo.domain.ReceitaPrescricao;
import pt.ipp.isep.labdsoft.Estudo.dto.ReceitaPrescricaoDTO;
import pt.ipp.isep.labdsoft.Estudo.persistence.ReceitaPrescricaoRepository;

@Service
public class ReceitaPrescricaoServiceImpl implements ReceitaPrescricaoService {

    private final ReceitaPrescricaoRepository receitaPrescricaoRepository;

    public ReceitaPrescricaoServiceImpl(ReceitaPrescricaoRepository repository){
        this.receitaPrescricaoRepository = repository;
    }

    @Override
    public ReceitaPrescricaoDTO save(ReceitaPrescricaoDTO dto) {
        ReceitaPrescricao rp = ReceitaPrescricao.fromDTO(dto);
        return receitaPrescricaoRepository.save(rp).toDTO();
    };
}
